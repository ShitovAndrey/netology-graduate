#!/bin/bash
ANSIBLE_CMD="ansible-playbook -i /inventory/inventory.ini --private-key /root/.ssh/id_ed25519 --become --become-user=root --user=cloud-user cluster.yml"

podman pull quay.io/kubespray/kubespray:v2.24.1
podman run --rm -it --mount type=bind,source=/home/andy/netology/git/netology-graduate/kubespray/inventory/,dst=/inventory \
--mount type=bind,source="${HOME}"/.ssh/id_ed25519,dst=/root/.ssh/id_ed25519 \
 quay.io/kubespray/kubespray:v2.24.1 ${ANSIBLE_CMD}