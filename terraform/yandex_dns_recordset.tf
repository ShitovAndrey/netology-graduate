# ===============================================================
#                   Создание DNS записей
# ===============================================================
resource "yandex_dns_zone" "avshitov" {
  name             = "avshitov"
  zone             = "redlinx.ru."
  public           = true
  private_networks = [module.vpc.yandex_vpc_network.id]
}

resource "yandex_dns_recordset" "app" {
  zone_id = yandex_dns_zone.avshitov.id
  name    = "app.redlinx.ru."
  type    = "A"
  ttl     = 200
  data    = ["${yandex_vpc_address.app-addr.external_ipv4_address[0].address}"]
}

resource "yandex_dns_recordset" "grafana" {
  zone_id = yandex_dns_zone.avshitov.id
  name    = "grafana.redlinx.ru."
  type    = "A"
  ttl     = 200
  data    = ["${yandex_vpc_address.app-addr.external_ipv4_address[0].address}"]
}

resource "yandex_dns_recordset" "api" {
  zone_id = yandex_dns_zone.avshitov.id
  name    = "api.redlinx.ru."
  type    = "A"
  ttl     = 200
  data    = ["${yandex_vpc_address.k8s-addr.external_ipv4_address[0].address}"]
}