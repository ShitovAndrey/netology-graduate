# ===============================================================
#                   Выделение ip адресов
# ===============================================================
resource "yandex_vpc_address" "app-addr" {
  name = "app-addr"
  deletion_protection = true

  external_ipv4_address {
    zone_id = "ru-central1-a"
  }
}

resource "yandex_vpc_address" "k8s-addr" {
  name = "k8s-addr"
  deletion_protection = true
  
  external_ipv4_address {
    zone_id = "ru-central1-a"
  }
}