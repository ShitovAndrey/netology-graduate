# ===============================================================
#                   Создание подсетей
# ===============================================================

# Создать пустую VPC. Выбрать зону.
module "vpc" {
  source = "./modules/yandex_vpc_network"
  vpc_name = "vpc_netology"

  providers = {
    yandex = yandex.yandex
  }
}

# Создать в VPC subnet с названием public, сетью 192.168.10.0/24.
module "subnet_public" {
  source = "./modules/yandex_vpc_subnet"
  
  subnet_name    = "public"
  subnet_zone    = var.default_zone
  vpc_network_id = module.vpc.yandex_vpc_network.id 
  subnet_v4_cidr_blocks = ["192.168.10.0/24"]

  providers = {
    yandex = yandex.yandex
  }

  depends_on = [
    module.vpc
  ]
}

# Создать в VPC subnet с названием private, сетью 192.168.20.0/24.
module "subnet_private" {
  source = "./modules/yandex_vpc_subnet"
  
  subnet_name    = "private"
  subnet_zone    = var.default_zone
  vpc_network_id = module.vpc.yandex_vpc_network.id 
  subnet_v4_cidr_blocks = ["192.168.20.0/24"]
  subnet_route_table_id = module.route_table.yandex_vpc_route_table.id

  providers = {
    yandex = yandex.yandex
  }

  depends_on = [
    module.vpc,
    module.route_table
  ]
}

# ===============================================================
#                   Создание виртуальных машин
# ===============================================================
# Создать в этой подсети NAT-инстанс, присвоив ему адрес 192.168.10.254. В качестве image_id использовать fd80mrhj8fl2oe87o4e1.
module "nat_instance" {
  source = "./modules/yandex_compute_instance"
  
  instance_name       = "nx-nat01"
  instance_zone       = var.default_zone
  instance_cores      = 2
  instance_memory     = 2
  instance_nat        = true
  instance_ipv4       = true
  instance_ip_address = "192.168.10.254"
  instance_image_id   = "fd80mrhj8fl2oe87o4e1"
  instance_subnet_id  = module.subnet_public.yandex_vpc_subnet.id 

  providers = {
    yandex = yandex.yandex
  }

  depends_on = [
    module.subnet_public
  ]
}

module "bastion_instance" {
  count = 1
  source = "./modules/yandex_compute_instance"
  
  instance_name   = "nx-jmp0${count.index + 1}"
  instance_zone   = var.default_zone
  instance_cores  = 2
  instance_memory = 4
  instance_nat    = true
  instance_ipv4   = true
  instance_ip_address = "192.168.10.100"
  instance_image_id  = "fd8e8q6sjccuijiqs9t1" #CentOS8 Stream
  instance_subnet_id = module.subnet_public.yandex_vpc_subnet.id 

  providers = {
    yandex = yandex.yandex
  }

  depends_on = [
    module.subnet_public
  ]
}

module "master_instance" {
  count = 1
  source = "./modules/yandex_compute_instance"
  
  instance_name   = "nx-k8sm0${count.index + 1}"
  instance_zone   = var.default_zone
  instance_cores  = 2
  instance_memory = 4
  instance_nat    = false
  instance_ipv4   = true

  instance_image_id  = "fd8e8q6sjccuijiqs9t1" #CentOS8 Stream
  instance_subnet_id = module.subnet_private.yandex_vpc_subnet.id

  providers = {
    yandex = yandex.yandex
  }

  depends_on = [
    module.subnet_private
  ]
}

module "worker_instance" {
  count = 2
  source = "./modules/yandex_compute_instance"
  
  instance_name   = "nx-k8sw0${count.index + 1}"
  instance_zone   = var.default_zone
  instance_cores  = 2
  instance_memory = 4
  instance_nat    = false
  instance_ipv4   = true

  instance_image_id  = "fd8e8q6sjccuijiqs9t1" #CentOS8 Stream
  instance_subnet_id = module.subnet_private.yandex_vpc_subnet.id

  providers = {
    yandex = yandex.yandex
  }

  depends_on = [
    module.subnet_private
  ]
}

# ===============================================================
#                   Создание правил маршрутизации
# ===============================================================
# Создать route table. Добавить статический маршрут, направляющий весь исходящий трафик private сети в NAT-инстанс.
module "route_table" {
  source = "./modules/yandex_vpc_route_table"
    
  vpc_network_id = module.vpc.yandex_vpc_network.id 
  route_destination_prefix = "0.0.0.0/0"
  route_next_hop_address   = "192.168.10.254"
  
  providers = {
    yandex = yandex.yandex
  }

  depends_on = [
    module.vpc
  ]
}

# ===============================================================
#                   Создание балансировщиков
# ===============================================================
resource "yandex_lb_target_group" "control-plane" {
  name      = "control-plane"
  region_id = "ru-central1"

  target {
    subnet_id = module.subnet_private.yandex_vpc_subnet.id
    address   = module.master_instance[0].yandex_compute_instance.network_interface[0].ip_address
  }

  depends_on = [
    module.subnet_private,
    module.master_instance
  ]
}

resource "yandex_lb_target_group" "worker-nodes" {
  name      = "worker-nodes"
  region_id = "ru-central1"

  dynamic "target" {
    for_each = module.worker_instance
    content {
      subnet_id = module.subnet_private.yandex_vpc_subnet.id
      address   = target.value.yandex_compute_instance.network_interface[0].ip_address
    }
  }

  depends_on = [
    module.subnet_private,
    module.worker_instance
  ]
}

resource "yandex_lb_network_load_balancer" "nlb-control-plane" {
  name = "nlb-control-plane"

  listener {
    name = "listner6443"
    protocol = "tcp"
    port = 6443
    external_address_spec {
      address = yandex_vpc_address.k8s-addr.external_ipv4_address[0].address
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.control-plane.id

    healthcheck {
      name = "tcp6443"
      tcp_options {
        port = 6443
      }
      timeout  = 10
      interval = 20
    }
  }
}

resource "yandex_lb_network_load_balancer" "nlb-worker-nodes" {
  name = "nlb-worker-nodes"

  listener {
    name = "listner80"
    protocol = "tcp"
    port = 80
    external_address_spec {
      address = yandex_vpc_address.app-addr.external_ipv4_address[0].address
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.worker-nodes.id

    healthcheck {
      name = "tcp80"
      tcp_options {
        port = 80
      }
      timeout  = 10
      interval = 20
    }
  }
}