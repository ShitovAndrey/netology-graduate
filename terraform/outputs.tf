resource "local_file" "inventory_ini" {
  content = templatefile("${path.module}/inventory.ini.tftpl",
    {
        master  =  module.master_instance[*].yandex_compute_instance
        worker  =  module.worker_instance[*].yandex_compute_instance
        bastion =  module.bastion_instance[*].yandex_compute_instance
        vip     =  yandex_vpc_address.k8s-addr.external_ipv4_address[0].address
    }
  )
  filename = "${abspath(path.module)}/../kubespray/inventory/inventory.ini"
}

locals {
  output_master = [
    for inst in module.master_instance : {
      fqdn   = inst.yandex_compute_instance.fqdn
      id     = inst.yandex_compute_instance.id
      name   = inst.yandex_compute_instance.name
      ip     = inst.yandex_compute_instance.network_interface[0].ip_address
      ip_nat = inst.yandex_compute_instance.network_interface[0].nat_ip_address
    }
  ]

  output_worker = [
    for inst in module.worker_instance : {
      fqdn   = inst.yandex_compute_instance.fqdn
      id     = inst.yandex_compute_instance.id
      name   = inst.yandex_compute_instance.name
      ip     = inst.yandex_compute_instance.network_interface[0].ip_address
      ip_nat = inst.yandex_compute_instance.network_interface[0].nat_ip_address
    }
  ]

  output_nat = [
    {
      fqdn   = module.nat_instance.yandex_compute_instance.fqdn
      id     = module.nat_instance.yandex_compute_instance.id
      name   = module.nat_instance.yandex_compute_instance.name
      ip     = module.nat_instance.yandex_compute_instance.network_interface[0].ip_address
      ip_nat = module.nat_instance.yandex_compute_instance.network_interface[0].nat_ip_address
    }
  ]

  output_bastion = [
    for inst in module.bastion_instance : {
      fqdn   = inst.yandex_compute_instance.fqdn
      id     = inst.yandex_compute_instance.id
      name   = inst.yandex_compute_instance.name
      ip     = inst.yandex_compute_instance.network_interface[0].ip_address
      ip_nat = inst.yandex_compute_instance.network_interface[0].nat_ip_address
    }
  ]
}

output "vm_output" { 
  value = flatten([local.output_master, local.output_worker, local.output_nat, local.output_bastion])
}