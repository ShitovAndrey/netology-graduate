# Дипломный практикум в Yandex.Cloud
  * [Цели:](#цели)
  * [Этапы выполнения:](#этапы-выполнения)
     * [Создание облачной инфраструктуры](#создание-облачной-инфраструктуры)
     * [Создание Kubernetes кластера](#создание-kubernetes-кластера)
     * [Создание тестового приложения](#создание-тестового-приложения)
     * [Подготовка cистемы мониторинга и деплой приложения](#подготовка-cистемы-мониторинга-и-деплой-приложения)
     * [Установка и настройка CI/CD](#установка-и-настройка-cicd)
  * [Что необходимо для сдачи задания?](#что-необходимо-для-сдачи-задания)
  * [Как правильно задавать вопросы дипломному руководителю?](#как-правильно-задавать-вопросы-дипломному-руководителю)

**Перед началом работы над дипломным заданием изучите [Инструкция по экономии облачных ресурсов](https://github.com/netology-code/devops-materials/blob/master/cloudwork.MD).**

---
## Что необходимо для сдачи задания?

1. Репозиторий с конфигурационными файлами Terraform и готовность продемонстрировать создание всех ресурсов с нуля.

[Ссылка на файлы terraform](./terraform/)

2. Пример pull request с комментариями созданными atlantis'ом или снимки экрана из Terraform Cloud или вашего CI-CD-terraform pipeline.

![](./images/terraform-pipeline1.png)  
![](./images/terraform-pipeline2.png)  
![](./images/terraform-pipeline3.png)  

3. Репозиторий с конфигурацией ansible, если был выбран способ создания Kubernetes кластера при помощи ansible.

[Ссылка на файлы kubespray](./kubespray/)

4. Репозиторий с Dockerfile тестового приложения и ссылка на собранный docker image.

Ссылка на репозиторий с приложением: [netology-application](https://gitlab.com/ShitovAndrey/netology-application/-/tree/main)  
Ссылка на docker image: [graduatework/graduate:1.0.1](https://hub.docker.com/layers/graduatework/graduate/1.0.1/images/sha256-9b817bc049a4a14592abb2dd663f757aa268bd9c8a25c1b28373b9cdd163bbbc?context=repo)  
Ссылка на pipeline в котором создался образ и задеплоился в кластер: [pipelines/1241106351](https://gitlab.com/ShitovAndrey/netology-application/-/pipelines/1241106351)  

5. Репозиторий с конфигурацией Kubernetes кластера.

[Ссылка на файлы kube-prometheus](./kube-prometheus/)

6. Ссылка на тестовое приложение и веб интерфейс Grafana с данными доступа.

Ссылка на тестовое приложение: [app.redlinx.ru](http://app.redlinx.ru)  
Ссылка на Grafana: [grafana.redlinx.ru](http://grafana.redlinx.ru)  

7. Все репозитории рекомендуется хранить на одном ресурсе (github, gitlab)