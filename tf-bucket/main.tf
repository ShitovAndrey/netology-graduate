terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.default_zone
}

// Создание бакета с использованием ключа
resource "yandex_storage_bucket" "tfbucket" {
  access_key = var.access_key
  secret_key = var.secret_key
  bucket     = "ntlbucket"
}